var Api = (function () {
    "use strict";

    var Api = function () {
    };

    Api.prototype = {
        /**
         * Global loading on ajax
         */
        globalLoader: function () {
            var $loader = $('#global-loader');
            $loader.hide();
            $(document).ajaxStart(function() {
                $loader.show();
            }).ajaxStop(function () {
                $loader.hide();
            });
        },

        handleEvents: function () {
            $(document).on('submit', '#send-url-form', function (e) {
                e.preventDefault();
                Api.sendUrl($(this));
            });
        }
    };

    Api.prototype.globalLoader();
    Api.prototype.handleEvents();

    /**
     * @param type alert|success|error|warning|information|confirmation
     * @param message
     */
    Api.showMessage = function (type, message) {
        noty({
            text: message,
            layout: 'topRight',
            type: type,
            closeWith: ['click', 'button'],
            timeout: 10000
        });
    };

    /**
     * @param $form string
     */
    Api.sendUrl = function ($form) {
        var url = $form.find('input:text').val();
        if (!url) {
            Api.showMessage('error', 'Url is empty. Please, paste url in the form field.');
            return;
        }

        $.ajax({
            url: $form.attr('action'),
            dataType: 'json',
            type: 'POST',
            data: { url: url }
        }).done(function (data, textStatus, jqXHR) {
            if (data.message) {
                Api.showMessage('info', data.message);
            }

            $.fileDownload(data.result.url, {
                failCallback: function (html, url) {
                    Api.showMessage('error', "Error to download file. Try again or notify the administrator");
                }
            });
        }).fail(function (jqXHR, textStatus, errorThrown) {
            var data = JSON.parse(jqXHR.responseText);
            Api.showMessage('error', data.error.message);
        });
    };


    return Api;
})();