<?php

// configure your app for the production environment

$app['twig.path'] = [__DIR__.'/../views'];
$app['twig.options'] = ['cache' => __DIR__.'/../var/cache/twig'];

$app['app.name'] = 'Video Loader';

$app['uploads.path'] = __DIR__.'/../web/uploads';
// 104857600 bytes - 100 Mb
$app['uploads.max.bytes'] = (1024 * 1024) * 100;

$app['phantomjs.path'] = __DIR__.'/../bin/phantomjs';
$app['phantomjs.loader.path'] = __DIR__.'/../bin/phantomloader';

$app['phantomjs.client'] = function () use ($app) {
    $Client = \JonnyW\PhantomJs\Client::getInstance();
    $Client->setPhantomJs($app['phantomjs.path']);
    $Client->setPhantomLoader($app['phantomjs.loader.path']);

    return $Client;
};
