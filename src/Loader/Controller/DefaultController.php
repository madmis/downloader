<?php

namespace Loader\Controller;

use HttpLib\Http;
use Loader\Util\FileUtil;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class DefaultController
 * @package Loader\Controller
 */
class DefaultController extends AbstractController
{
    /**
     * @return Response
     */
    public function indexAction()
    {
        $view = $this->getView()->render('index.html.twig', []);

        return new Response($view);
    }

    public function aboutAction()
    {
        $view = $this->getView()->render('about.html.twig', []);

        return new Response($view);
    }

    /**
     * @param Request $request
     * @param string $fileName
     * @return BinaryFileResponse
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function getFileAction(Request $request, $fileName)
    {
        $path = "{$this->getUploadsPath()}/{$fileName}";

        if (!file_exists($path)) {
            throw new HttpException(Http::CODE_NOT_FOUND, "Can't find video file {$fileName}");
        }

        $Response = new BinaryFileResponse($path);
        $Response->headers->set('Content-Type', FileUtil::getMimeType($path));
        $Response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $fileName);

        return $Response;
    }

} 