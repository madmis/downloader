<?php

namespace Loader\Controller;

use HttpLib\Http;
use Loader\Service\Loader;
use Loader\Util\FileUtil;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ApiController extends AbstractApiController
{
    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function sendUrlAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException('Page not found.');
        }

        $url = $request->request->get('url');
        if (empty($url)) {
            return $this->apiError('Url is required.');
        }

        if (filter_var($url, FILTER_VALIDATE_URL) === false) {
            return $this->apiError('Invalid url. Please, fix it and try again.');
        }

        try {
            $Loader = new Loader($this->getClient(), $url, $this->getUploadsPath());
            $fileSize = $Loader->getService()->getFileSize();
        } catch (\Exception $ex) {
            return $this->apiError($ex->getMessage());
        }

        if ($fileSize > $this->getUploadsMaxBytes()) {
            $size = FileUtil::bytesToMb($fileSize);
            $allowed = $this->getUploadsMaxMegaBytes();
            return $this->apiError("File too big. File size: {$size} Mb. Allowed size: {$allowed} Mb.");
        }

        try {
            $fileName = $Loader->getService()->saveFile();
        } catch (\Exception $ex) {
            return $this->apiError($ex->getMessage());
        }

        $url = $this->getUrlGenerator()->generate('get_file', ['fileName' => $fileName]);

        return $this->apiSuccess(['url' => $url], Http::CODE_OK);
    }
}