<?php

namespace Loader\Controller;
use HttpLib\Http;
use JonnyW\PhantomJs\ClientInterface;
use Loader\Util\FileUtil;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Generator\UrlGenerator;

/**
 * Class AbstractApiController
 * @package Loader\Controller
 */
class AbstractApiController
{
    /**
     * @var string
     */
    private $uploadsPath;

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var UrlGenerator
     */
    private $urlGenerator;

    /**
     * @var int
     */
    private $uploadsMaxBytes;

    /**
     * @param ClientInterface $Client
     * @param UrlGenerator $urlGenerator
     * @param string $uploadsPath
     * @param int $uploadsMaxBytes
     */
    public function __construct(ClientInterface $Client, UrlGenerator $urlGenerator, $uploadsPath, $uploadsMaxBytes)
    {
        $this->client = $Client;
        $this->urlGenerator = $urlGenerator;
        $this->uploadsPath = $uploadsPath;
        $this->uploadsMaxBytes = $uploadsMaxBytes;
    }

    /**
     * @return string
     */
    protected function getUploadsPath()
    {
        return $this->uploadsPath;
    }

    /**
     * @return \JonnyW\PhantomJs\ClientInterface
     */
    protected function getClient()
    {
        return $this->client;
    }

    /**
     * @return \Symfony\Component\Routing\Generator\UrlGenerator
     */
    protected function getUrlGenerator()
    {
        return $this->urlGenerator;
    }

    /**
     * @return int
     */
    public function getUploadsMaxBytes()
    {
        return $this->uploadsMaxBytes;
    }

    /**
     * @return float
     */
    public function getUploadsMaxMegaBytes()
    {
        return FileUtil::bytesToMb($this->uploadsMaxBytes);
    }

    /**
     * Api error response
     * @param string $message
     * @param int $code http code
     * @return JsonResponse
     */
    protected function apiError($message, $code = Http::CODE_BAD_REQUEST)
    {
        $data = [
            'error' => [
                'code' => $code,
                'message' => $message
            ]
        ];

        return new JsonResponse($data, $code);
    }

    /**
     * Api success response
     * @param array $result
     * @param int $code
     * @param string $message
     * @return JsonResponse
     */
    protected function apiSuccess(array $result, $code = Http::CODE_OK, $message = '')
    {
        $data = ['result' => $result];
        if ($message) {
            $data['message'] = $message;
        }

        return new JsonResponse($data, $code);
    }

} 