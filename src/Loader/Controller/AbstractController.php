<?php

namespace Loader\Controller;

/**
 * Class AbstractController
 * @package Loader\Controller
 */
class AbstractController
{
    /**
     * @var \Twig_Environment
     */
    private $view;

    /**
     * @var string
     */
    private $uploadsPath;

    /**
     * @param \Twig_Environment $view
     * @param string $uploadsPath
     */
    public function __construct(\Twig_Environment $view, $uploadsPath)
    {
        $this->view = $view;
        $this->uploadsPath = $uploadsPath;
    }

    /**
     * @return \Twig_Environment
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @return string
     */
    public function getUploadsPath()
    {
        return $this->uploadsPath;
    }
}