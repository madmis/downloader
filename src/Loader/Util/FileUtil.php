<?php

namespace Loader\Util;

/**
 * Class FileUtil
 * @package Loader\Util
 */
class FileUtil
{
    /**
     * @param int $bytes
     * @return float
     */
    public static function bytesToKb($bytes)
    {
        return (float)number_format($bytes / 1024, 2);
    }

    /**
     * @param int $bytes
     * @return float
     */
    public static function bytesToMb($bytes)
    {
        return (float)number_format($bytes / 1048576, 2);
    }

    /**
     * @param int $bytes
     * @return float
     */
    public static function bytesToGb($bytes)
    {
        return (float)number_format($bytes / 1073741824, 2);
    }

    /**
     * @param string $pathToFile
     * @return string
     */
    public static function getMimeType($pathToFile)
    {
        $info = finfo_open(FILEINFO_MIME_TYPE);
        $mime = finfo_file($info, $pathToFile);
        finfo_close($info);

        return $mime;
    }
}