<?php

namespace Loader\Util;

/**
 * Class UrlUtil
 * @package Loader\Util
 */
class UrlUtil {

    /**
     * Add protocol to url, if not exists
     * @param string $url
     * @param string $protocol
     * @return string
     */
    public static function addProtocol($url, $protocol = 'http://')
    {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = $protocol . $url;
        }

        return $url;
    }

    /**
     * Get host from url (without sub domains)
     * @param string $url
     * @return string
     * @throws \RuntimeException
     */
    public static function getHostFromUrl($url)
    {
        $host = parse_url($url, PHP_URL_HOST);
        preg_match("#[^\.\/]+\.[^\.\/]+$#", $host, $matches);

        if (empty($matches[0])) {
            throw new \RuntimeException("Error to parse url: {$url}");
        }

        return $matches[0];
    }
}