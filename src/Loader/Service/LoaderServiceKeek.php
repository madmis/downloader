<?php

namespace Loader\Service;

use HttpLib\Http;
use JonnyW\PhantomJs\ClientInterface;

/**
 * Class LoaderServiceKeek
 * @package Loader\Service
 */
class LoaderServiceKeek implements LoaderServiceInterface
{
    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $videoUrl;

    /**
     * @var string
     */
    private $videoType;

    /**
     * @var string
     */
    private $uploadsPath;

    /**
     * @param ClientInterface $Client
     * @param string $url
     * @param string $uploadsPath
     */
    public function __construct(ClientInterface $Client, $url, $uploadsPath)
    {
        $this->client = $Client;
        $this->url = $url;
        $this->uploadsPath = $uploadsPath;
        $this->findVideoUrl();
    }

    /**
     * Get remote file size in bytes
     * @return int
     */
    public function getFileSize()
    {
        $ch = curl_init($this->videoUrl);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, TRUE);
        curl_setopt($ch, CURLOPT_NOBODY, TRUE);

        curl_exec($ch);
        $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);

        curl_close($ch);
        return $size;
    }

    private function findVideoUrl()
    {
        /** @see JonnyW\PhantomJs\Message\Request **/
        $request = $this->client->getMessageFactory()
            ->createRequest($this->url, Http::METHOD_GET, 10000);
        /** @see JonnyW\PhantomJs\Message\Response **/
        $response = $this->client->getMessageFactory()->createResponse();
        $delay = 5; // 5 seconds
        $request->setDelay($delay);
        $this->client->send($request, $response);

        if($response->getStatus() === Http::CODE_OK) {
            // Dump the requested page content
            $content = $response->getContent();
        }

        if (empty($content)) {
            throw new \RuntimeException("Error to parse page {$this->url}");
        }

        /** @var \DOMDocument $Dom */
        $Dom = \HTML5_Parser::parse($content);
        $tags = $Dom->getElementsByTagName('video');

        foreach ($tags as $tag) {
            /** @var $tag \DOMElement */
            $child = $tag->getElementsByTagName('source');
            if ($child->length > 0) {
                foreach ($child as $childTag) {
                    /** @var $childTag \DOMElement */
                    if ($childTag->hasAttribute('src')) {
                        $this->videoUrl = $childTag->getAttribute('src');
                        $this->videoType = $childTag->getAttribute('type');
                        break;
                    }
                }
            }
        }

        if (empty($this->videoType)) {
            throw new \RuntimeException("Error to find video url");
        }
    }

    /**
     * Service name
     * @return string
     */
    public function getServiceName()
    {
        return 'keek';
    }

    /**
     * Get source url
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get video url. Can be different from source url
     * @return string
     */
    public function getVideoUrl()
    {
        return $this->url;
    }

    /**
     * Save video to disk
     * @return string file name
     */
    public function saveFile()
    {
        $name = md5(microtime());
        $ext = explode('/', $this->videoType);
        $name .= '.' . $ext[1];

        $content = file_get_contents($this->videoUrl);
        file_put_contents("{$this->uploadsPath}/{$name}", $content);

        return $name;
    }
}