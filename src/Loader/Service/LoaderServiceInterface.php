<?php

namespace Loader\Service;

/**
 * Interface LoaderServiceInterface
 * @package Loader\Service
 */
interface LoaderServiceInterface
{
    /**
     * Get size of downloaded file
     * @return int
     */
    public function getFileSize();

    /**
     * Service name
     * @return string
     */
    public function getServiceName();

    /**
     * Get source url
     * @return string
     */
    public function getUrl();

    /**
     * Get video url. Can be different from source url
     * @return string
     */
    public function getVideoUrl();

    /**
     * Save file to disk
     * @return string path to file
     */
    public function saveFile();
}