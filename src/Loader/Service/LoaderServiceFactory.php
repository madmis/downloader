<?php

namespace Loader\Service;

use JonnyW\PhantomJs\ClientInterface;
use Loader\Util\UrlUtil;

class LoaderServiceFactory
{
    /**
     * @var array
     */
    private static $services = [
        'keek' => ['keek.com', 'akamaihd.net',],
    ];

    /**
     * @param ClientInterface $Client
     * @param string $url
     * @param string $uploadsPath
     * @return LoaderServiceInterface
     */
    public static function getService(ClientInterface $Client, $url, $uploadsPath)
    {
        $serviceName = self::getServiceName($url);
        return self::createService($Client, $url, $serviceName, $uploadsPath);
    }

    /**
     * @param ClientInterface $Client
     * @param string $url
     * @param string $serviceName
     * @param string $uploadsPath
     * @return LoaderServiceInterface
     */
    private static function createService(ClientInterface $Client, $url, $serviceName, $uploadsPath)
    {
        switch ($serviceName) {
            case 'keek':
            default;
                return new LoaderServiceKeek($Client, $url, $uploadsPath);
                break;
        }
    }

    /**
     * @param string $url
     * @return string
     * @throws \RuntimeException
     */
    private static function getServiceName($url)
    {
        $host = UrlUtil::getHostFromUrl($url);

        foreach (self::$services as $name => $hosts) {
            if (in_array($host, $hosts)) {
                return $name;
            }
        }

        throw new \RuntimeException("Error to define service name by url: {$url}");
    }
}