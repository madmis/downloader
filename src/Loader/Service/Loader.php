<?php

namespace Loader\Service;
use JonnyW\PhantomJs\ClientInterface;
use Loader\Util\UrlUtil;

/**
 * Class Loader
 * @package Loader\Service
 */
class Loader {

    /**
     * @var string
     */
    private $url;

    /**
     * @var LoaderServiceInterface
     */
    private $service;

    /**
     * @param ClientInterface $Client
     * @param string $url
     * @param string $uploadsPath
     */
    public function __construct(ClientInterface $Client, $url, $uploadsPath)
    {
        $this->prepareUrl($url);
        $this->setService($Client, $uploadsPath);
    }

    /**
     * @return LoaderServiceInterface
     */
    public function getService()
    {
        return $this->service;
    }


    /**
     * Add protocol to url, if it not exists
     * @param string $url
     */
    private function prepareUrl($url)
    {
        $this->url = UrlUtil::addProtocol($url);
    }

    /**
     * @param ClientInterface $Client
     * @param string $uploadsPath
     */
    private function setService(ClientInterface $Client, $uploadsPath)
    {
        $this->service = LoaderServiceFactory::getService($Client, $this->url, $uploadsPath);
    }
}