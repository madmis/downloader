<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @var $app \Silex\Application
 */

$app->register(new Silex\Provider\ServiceControllerServiceProvider());

//Request::setTrustedProxies(array('127.0.0.1'));
$app['default.controller'] = $app->share(function() use ($app) {
    return new \Loader\Controller\DefaultController($app['twig'], $app['uploads.path']);
});
$app['api.controller'] = $app->share(function() use ($app) {
    return new \Loader\Controller\ApiController(
        $app['phantomjs.client'],
        $app['url_generator'],
        $app['uploads.path'],
        $app['uploads.max.bytes']
    );
});

$app->get('/', 'default.controller:indexAction')->bind('homepage');
$app->get('/about', 'default.controller:aboutAction')->bind('about');
$app->post('/api/sendUrl', 'api.controller:sendUrlAction')->bind('post_api_send_url');
$app->get('/file/{fileName}', 'default.controller:getFileAction')->assert('fileName', '.*')->bind('get_file');

$app->error(function (\Exception $ex, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    $templates = [
        4 => "errors/4xx.html.twig",
        5 => "errors/5xx.html.twig",
    ];

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $template = 'errors/default.html.twig';
    $codePart = substr($code, 0, 1);
    if (isset($templates[$codePart])) {
        $template = $templates[$codePart];
    }

    $view = $app['twig']->render($template, ['code' => $code, 'message' => $ex->getMessage()]);
    return new Response($view, $code);
});
