<?php

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

$console = new Application('Video Loader', '1.0');
$console->getDefinition()->addOption(new InputOption('--env', '-e', InputOption::VALUE_REQUIRED, 'The Environment name.', 'dev'));
$console->setDispatcher($app['dispatcher']);
$console
    ->register('loader:uploads:clear')
    ->setDefinition([
         new InputOption(
             'max-time', 't', InputOption::VALUE_OPTIONAL,
             'Max time from video uploads in minutes. If video file time more than this time, delete video ', 20
         ),
    ])
    ->setDescription('Command to delete old video from uploads directory.')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        $time = $input->getOption('max-time');

        $output->writeln('Start command: '  . date('Y-m-d H:i:s', time()));

        $Finder = new \Symfony\Component\Finder\Finder();
        $Finder->files()->in($app['uploads.path'])->ignoreDotFiles(true)->date("until {$time} minutes ago");

        foreach ($Finder as $File) {
            /** @var \Symfony\Component\Finder\SplFileInfo $File */
            if ($File->isFile()) {
                $output->writeln('Delete file: ' . $File->getRealPath());
                unlink($File->getRealPath());
            }
        }
    });

return $console;
